import logging
import re
from _ast import Call

from aiogram import Bot, Dispatcher, executor, types
from aiogram.dispatcher import FSMContext
from aiogram.contrib.fsm_storage.files import JSONStorage
from aiogram.dispatcher.filters.state import StatesGroup, State
from aiogram.types import ReplyKeyboardMarkup, InlineKeyboardMarkup, InlineKeyboardButton, InputMediaPhoto

from .messages import messages as msgs, keyboards as kbs
from .settings import API_TOKEN


# Configure logging

def get_intent(text):
    return "UNKNOWN"


logging.basicConfig(level=logging.DEBUG)

# Initialize bot and dispatcher

storage = JSONStorage("storage.json")

bot = Bot(token=API_TOKEN)

dp = Dispatcher(bot, storage=storage)


class States(StatesGroup):
    main_state = State()


@dp.message_handler(commands=['start', 'help'], state="*")
async def send_welcome(message: types.Message, state: FSMContext):
    await States.main_state.set()
    await message.reply(msgs.get_message("hello"), reply_markup=kbs.get_keyboard("main.keyboard"))
    logging.info(f"Message from {message.from_user.username}: {message.text}")


# @dp.message_handler(lambda msg: get_intent(msg.text) != "UNKNOWN", state=States.main_state)
# async def has_intent_handler(message: types.Message, state: FSMContext):
#     await message.answer(get_intent(message.text))
#
#
# @dp.message_handler(state=States.main_state)
# async def default_handler(message: types.Message, state: FSMContext):
#     await message.answer("СОВСЕМ НЕПОНЯТНО")

@dp.message_handler(state=States.main_state)
async def default_handler(message: types.Message, state: FSMContext):
    intent = get_intent(message.text)
    if intent != "UNKNOWN":
        await message.answer(intent)
    else:
        await message.answer("СОВСЕМ НЕПОНЯТНО")


def run_bot(_get_intent=None):
    if _get_intent is not None:
        global get_intent
        get_intent = _get_intent
    executor.start_polling(dp, skip_updates=True)
