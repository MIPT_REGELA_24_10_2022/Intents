from aiogram.types import ReplyKeyboardMarkup


class Keyboards:
    def __init__(self, keyboards: dict[str, str]):
        self.keyboards: dict[str, str] = keyboards

    def get_keyboard(self, keyboard_name):
        if keyboard_name in self.keyboards:
            return self.keyboards[keyboard_name]


main_keyboard = ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True) \
    .add('Список специалистов', 'Записаться на приём').add('Информация', 'Мои записи')

accept_keyboard = ReplyKeyboardMarkup().add("Принять", "Отменить")

keyboards_dict = {
    "main_keyboard": main_keyboard,
    "accept_keyboard": accept_keyboard,
}

keyboards = Keyboards(keyboards_dict)
