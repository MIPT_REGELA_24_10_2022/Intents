class Messages:
    def __init__(self, messages: dict[str, str]):
        self.messages: dict[str, str] = messages

    def get_message(self, message_name, *args, **kwargs):
        if message_name in self.messages:
            return self.messages[message_name].format(*args, **kwargs)


messages_dict = {
    "hello": "Здравствуйте!",
}

messages = Messages(messages_dict)
